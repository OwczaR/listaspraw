package com.example.listaspraw;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


/**
 * Klada agregujaca informacje o zadaniu.
 * @author Kamil
 * @version 201406302226
 */
public class Zadanie {
	private String tre��;
	private String nazwa;
	private Calendar data;
	private StanZadania stan;
	
	public String getTre��() {
		return tre��;
	}

	public void setTre��(String tre��) {
		this.tre�� = tre��;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public Calendar getData() {
		return data;
	}

	public void setData(Calendar data) {
		this.data = data;
	}

	public StanZadania getStan() {
		return stan;
	}
	void setStan(StanZadania stan) {
		this.stan = stan;
	}

	public Zadanie(String tre��, String nazwa, Calendar data, StanZadania stan) {
		super();
		this.tre�� = tre��;
		this.nazwa = nazwa;
		this.data = data;
		this.stan = stan;
	}

	@Override
	public String toString() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
		return stan.getNapis() + ": " + nazwa + "\n" + sdf.format(data.getTime());
	}	
}
