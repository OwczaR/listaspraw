package com.example.listaspraw;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Klasa rozszerzajaca klase SQLiteOpenHelper.
 * Ma umozliwiac laczenie sie z baza danych SQLite.
 * @author Kamil Owczarek
 * @version 201406302226
 */
public class Zarz�dcaSQLite extends SQLiteOpenHelper {

	public Zarz�dcaSQLite(Context context) {
		super(context, "ListaSpraw.db", null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE sprawy("
				+"id INTEGER PRIMARY KEY AUTOINCREMENT,"
				+"tytul TEXT,"
				+"data DATE,"
				+"tresc TEXT,"
				+"stan TEXT"
				+");");
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		
	}

	/**
	 * Metoda dodajZadanie
	 * Dodaje rekord symbolizujacy zadanie do bazy danych SQLite.
	 * @param zad
	 */
	public void dodajZadanie(Zadanie zad) {
		String tytul = zad.getNazwa(); 
		Date data = zad.getData().getTime(); 
		String tresc = zad.getTre��(); 
		String stan = zad.getStan().getNapis();
		SQLiteDatabase db = getWritableDatabase();
		ContentValues zawartosc = new ContentValues();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
		zawartosc.put("tytul", tytul);
		zawartosc.put("data", sdf.format(data));
		zawartosc.put("tresc", tresc);
		zawartosc.put("stan", stan);
		db.insertOrThrow("sprawy", null, zawartosc);
	}
	
	/**
	 * Metoda ma pobrac wszystkie rekordy z bazy danych.
	 * @return Obejkt klasy ArrayList<Zadanie> 
	 * zawierajacy zadania wczytane z bazy danych.
	 */
	public ArrayList<Zadanie> pobierzWszystkieDaneZBazy() {
		String kolumny[] = {"id", "tytul", "data", "tresc", "stan"};
		SQLiteDatabase db = getReadableDatabase();
		Cursor cursor =	db.query("sprawy", kolumny, null, null, null, null, null);
		
		ArrayList<Zadanie> zadania = new ArrayList<Zadanie>(); 
        while(cursor.moveToNext()) {
        	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        	String tytul = cursor.getString(1);
        	Date data = new Date();
			try {
				data = sdf.parse(cursor.getString(2));
			} catch (ParseException e) {
				e.printStackTrace();
			}
        	String tresc = cursor.getString(3);
        	String stan = cursor.getString(4);
        	Calendar calendar = new GregorianCalendar();
        	calendar.setTime(data);
        	zadania.add(new Zadanie(tresc, tytul, calendar, 
        			(stan.equals((new StanDoWykonania()).getNapis()))?(new StanDoWykonania()):(new StanWykonane())));
        }
        
        return zadania;
	}
	
	/**
	 * Metoda updatuje rekord w bazie danych.
	 * @param stare - zadanie ktore bedzie updatowane
	 * @param nowe - zadanie zawierajace nowe dane
	 */
	public void updateZadania(Zadanie stare, Zadanie nowe) {
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    	StringBuilder sb = new StringBuilder();
		ContentValues cv = new ContentValues();
		cv.put("tytul", nowe.getNazwa());
		cv.put("data", sdf.format(nowe.getData().getTime()));
		cv.put("tresc", nowe.getTre��());
		cv.put("stan", nowe.getStan().getNapis());
		sb.append("tytul like ");
		sb.append("'" + stare.getNazwa() + "'");
		sb.append(" and data = ");
		sb.append("'" + sdf.format(stare.getData().getTime()) + "'");
		sb.append(" and tresc like ");
		sb.append("'" + stare.getTre��() + "'");
		sb.append(" and stan like ");
		sb.append("'" + stare.getStan().getNapis() + "'");
		String where = sb.toString();
		SQLiteDatabase db = getWritableDatabase();
		db.update("sprawy", cv, where, null);
	}
	
	/**
	 * Metoda do usuwania konkretnego rekordu z bazy danych.
	 * @param zad - zadanie do usuniecia
	 */
	public void usunZadanie(Zadanie zad) {
		SQLiteDatabase db = getWritableDatabase();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
		StringBuilder sb = new StringBuilder();
		sb.append("tytul like ");
		sb.append("'" + zad.getNazwa() + "'");
		sb.append(" and data = ");
		sb.append("'" + sdf.format(zad.getData().getTime()) + "'");
		sb.append(" and tresc like ");
		sb.append("'" + zad.getTre��() + "'");
		sb.append(" and stan like ");
		sb.append("'" + zad.getStan().getNapis() + "'");
		String where = sb.toString();
		db.delete("sprawy", where, null);
	}
	
	/**
	 * Metoda usuwajaca wszystkie rekordy z bazy danych.
	 */
	public void usunWszystko() {
		SQLiteDatabase db = getWritableDatabase();
		db.execSQL("delete from sprawy;");
	}
}
