package com.example.listaspraw;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


/**
 * Klasa MojAdapter - adapter dla ListView
 * przechowujacej zadania. Wykorzystywany
 * w klasie ListaSpraw. Ta klasa umozliwia
 * wyswietlanie odpowiednich kolorow w liscie.
 * @author Kamil
 * @version 201406302226
 */
public class MojAdapter extends ArrayAdapter<Zadanie> {
    public MojAdapter(Context context, ArrayList<Zadanie> zadania) {
       super(context, R.layout.row, zadania);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
       Zadanie zad = getItem(position);
       if (convertView == null) {
           convertView = LayoutInflater.from(getContext()).inflate(R.layout.row, parent, false);
       }
       TextView view = (TextView) super.getView(position, convertView, parent);
       if(zad.getStan().equals((new StanDoWykonania()))){
    	   view.setTextColor(0xffcccccc); //zielony
       } else {
    	   view.setTextColor(0xff000000); //czarny
       }
       return (View) view;
   }
}
