package com.example.listaspraw;

/**
 * Interfejs - oznaczenie stanu zadania.
 * @author Kamil
 * @version 201406302226
 */
interface StanZadania {
	public String getNapis();
}