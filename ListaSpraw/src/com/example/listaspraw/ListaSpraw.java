package com.example.listaspraw;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Locale;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;
import android.view.ContextMenu;   
import android.view.ContextMenu.ContextMenuInfo; 


/**
 * ListaSpraw - klasa dziedziczaca po fragment,
 * odpowiada za wyswietlenie listy zadan aktualnie
 * zapisanych w systemie.
 * @author Kamil
 * @version 201406302226
 */
public class ListaSpraw extends Fragment {
	View rootView;
	static private ListView list;
	static private MojAdapter adapter;
	static private ArrayList<Zadanie> zadania;
	static private Zarz�dcaSQLite zs;
 
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_lista_spraw, container, false);
        list = (ListView) rootView.findViewById(R.id.ListView);
        zs = new Zarz�dcaSQLite(getActivity());
        zadania = zs.pobierzWszystkieDaneZBazy();
        adapter = new MojAdapter(getActivity(), zadania);
        list.setAdapter(adapter);
        registerForContextMenu(list);
        list.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				przejdzDoPodgladu(adapter.getItem(position));
			}
		}); 
        return rootView;
    }
    
    @SuppressWarnings("unused")
	static private void zapiszZadania() {
    	zs.usunWszystko();
    	for(Zadanie zad:zadania)
    		zs.dodajZadanie(zad);
    }
    
    /*@Override
    public void onDestroyView() {
    	super.onDestroy();
    	zs.usunWszystko();
    	for(Zadanie zad:zadania)
    		zs.dodajElement(zad);
    }*/
    
    /**
     * Funkcja wywoluje intencje przejscia do aktywnosci podgladu.
     * W widoku tym s� udostepniane informacje na temat konkretnego 
     * zadania z listy spraw.
     * @param zadanie - zadanie na rzecz ktorego wywolywana jest
     * intecja podgladu.
     */
    private void przejdzDoPodgladu(Zadanie zadanie) {
		Intent intent = new Intent(getActivity(), PodgladActivity.class);
		
		intent.putExtra("tytul", zadanie.getNazwa());
		if(zadanie.getData() instanceof GregorianCalendar) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
			intent.putExtra("data", sdf.format(((GregorianCalendar) zadanie.getData()).getTime()));
		}
		intent.putExtra("tresc", zadanie.getTre��());
		intent.putExtra("stan", zadanie.getStan().getNapis());
		
	    startActivity(intent);
	}
    
    /**
     * Metoda, ktora umozliwia przejscie 
     * do ekranu (aktywnosci edycji).
     * @param num - numer zadania, na rzecz, ktorego
     * wykonywane jest przejscie do ekranu edycji.
     */
    private void przejdzDoEdycji(int num) {
		Intent intent = new Intent(getActivity(), EdycjaActivity.class);
		
		intent.putExtra("numer", "" + num);
		
	    startActivity(intent);
	}
    
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,ContextMenuInfo menuInfo) { 
    	super.onCreateContextMenu(menu, v, menuInfo);  
    	    menu.setHeaderTitle("Co chcesz zrobi�?");  
    	    menu.add(0, v.getId(), 0, "Usu�");  
    	    menu.add(0, v.getId(), 0, "Edytuj");
    	    menu.add(0, v.getId(), 0, "Podgl�d");
    }  
    
    @Override
    public boolean onContextItemSelected(MenuItem item) { 
    	AdapterContextMenuInfo info = (AdapterContextMenuInfo)item.getMenuInfo();
    	
        if(item.getTitle()=="Usu�"){
        	usunZadanie(info.position);
        	Toast.makeText(getActivity(), "Usu�", Toast.LENGTH_SHORT).show();
        }  
        else if(item.getTitle()=="Edytuj"){
        	przejdzDoEdycji(info.position);
        	Toast.makeText(getActivity(), "Edytuj", Toast.LENGTH_SHORT).show();
        }  
        else if(item.getTitle()=="Podgl�d") {
        	przejdzDoPodgladu(getZadanie(info.position));
        	Toast.makeText(getActivity(), "Podgl�d", Toast.LENGTH_SHORT).show();
        }
        else {return false;}  
    
        return true;  
    }
    
    /**
     * dodajZadanie - metoda pozwalajaca na
     * dodanie nowego zadania w systemie.
     * @param zadanie - zadanie ktore ma 
     * zostac dodane do aktualnej listy.
     */
    static public void dodajZadanie(Zadanie zadanie) {
    	zadania.add(zadanie);
    	zs.dodajZadanie(zadanie);
    	odswiez();
    }
    
    /**
     * getZadanie - metoda dostepowa dla 
     * konkretnych zadan z listy.
     * @param position
     * @return Zadanie o wskazanym numerze.
     */
    static public Zadanie getZadanie(int position) {
    	return adapter.getItem(position);
    }
    
    /**
     * usunZadanie - usuwa zadanie o podanym
     * numerze z listy zadan.
     * @param i - numer zadania do usuniecia
     */
    static public void usunZadanie(int i) {
    	zs.usunZadanie(adapter.getItem(i));
    	zadania.remove(i);
    	odswiez();
    }
    
    /**
     * zmienZadanie - zamienia zadanie stare na nowe.
     * Typowa metoda updatujaca.
     * @param stare - zadanie ktore ma zostac zmienione.
     * @param nowe - zadanie aktualne.
     */
    static public void zmienZadanie(Zadanie stare, Zadanie nowe){
    	zs.updateZadania(stare, nowe);
    	int nr = zadania.indexOf(stare);
    	zadania.remove(stare);
    	zadania.add(nr, nowe);
    	odswiez();
    }
    
    /**
     * odswiez - odswieza stan ListView, 
     * uaktualnia wyswietlane informacje.
     */
    static public void odswiez() {
    	adapter.notifyDataSetChanged();
    	list.invalidateViews();
    }
}