package com.example.listaspraw;

import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * Klasa PodgladActivity - aktywnosc
 * umozliwiajaca dokladniejszy podglad zadania
 * z poziomu interfejsu uzytkownika.
 * Korzysta z layaout activity_podglad. 
 * @author Kamil
 * @version 201406302226
 */
public class PodgladActivity extends ActionBarActivity {
	static String tytulString;
	static String dataString;
	static String trescString;
	static String stanString;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_podglad);
		setStrings();
		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}
	
	private void setStrings() {
		Bundle bundle = getIntent().getExtras();
		tytulString = bundle.getString("tytul");
		dataString = bundle.getString("data");
		trescString = bundle.getString("tresc");
		stanString = bundle.getString("stan");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.podglad, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	
	/**
	 * Statyczna klasa wewnetrzna PlaceholderFragment -
	 * - Fragment przechowujacy wszystkie kontrolki dla
	 * tej aktywnosci, wspolpracuje z layoutem 
	 * fragment_podglad.
	 * @author Kamil
	 *
	 */
	public static class PlaceholderFragment extends Fragment {
		View rootView;
		TextView tytul;
		TextView data;
		TextView opis;
		TextView stan;

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			rootView = inflater.inflate(R.layout.fragment_podglad,
					container, false);
			
			initTextViews();
			setTexts();
			
			return rootView;
		}

		private void setTexts() {
			tytul.setText(tytulString);
			data.setText(dataString);
			opis.setText(trescString);
			stan.setText(stanString);
		}

		private void initTextViews() {
			tytul = (TextView) rootView.findViewById(R.id.tytul);
			data = (TextView) rootView.findViewById(R.id.data);
			opis = (TextView) rootView.findViewById(R.id.opis);
			stan = (TextView) rootView.findViewById(R.id.stanTextView);
		}
	}
}
