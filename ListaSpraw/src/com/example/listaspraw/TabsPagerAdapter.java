package com.example.listaspraw;
 
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
 
/**
 * Klasa Adapter na TabsPager, ktory znajduje sie
 * u gory glownej aktywnosci.
 * @author Kamil
 * @version 201406302226
 */
public class TabsPagerAdapter extends FragmentPagerAdapter {
	private static final int counter = 2;
 
    public TabsPagerAdapter(FragmentManager fm) {
        super(fm);
    }
 
    @Override
    public Fragment getItem(int index) {
        switch (index) {
        case 0:
            return new ListaSpraw();
        case 1:
            return new DodajNowa();
        }
        return null;
    }
 
    @Override
    public int getCount() {
        return counter;
    }
}