package com.example.listaspraw;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Locale;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
 
/**
 * DodajNowa - klasa dziedziczaca po klasie Fragment.
 * Umozliwia dodawanie do systemu nowych zadan.
 * Znajduje sie pod aktywnoscia MainActivity.
 * Wspolpracuje z fragment_dodaj_nowa.
 * @author Kamil
 * @version 201406302226
 */
public class DodajNowa extends Fragment {
	View rootView;
	TextView tytul;
	TextView data;
	TextView tresc;
	EditText tytulEdit;
	EditText dataEdit;
	EditText trescEdit;
	Button zapiszButton;
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_dodaj_nowa, container, false);
        initTextViews();
        initEditTexts();
        initZapiszButton();
        
        return rootView;
    }

    /**
	 * initZapiszButton - inicjalizacja oblusgi przycisku.
	 */
	private void initZapiszButton() {
		zapiszButton = (Button) rootView.findViewById(R.id.zapisz_button);
        zapiszButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
				GregorianCalendar gc = new GregorianCalendar();
				try {
					gc.setTime(sdf.parse(dataEdit.getText().toString()));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				Zadanie zadanie = new Zadanie(trescEdit.getText().toString(), 
						tytulEdit.getText().toString(), gc, new StanDoWykonania());
				ListaSpraw.dodajZadanie(zadanie);
			}
		});
	}
	
	private void initEditTexts() {
		tytulEdit = (EditText) rootView.findViewById(R.id.editTytul);
        dataEdit = (EditText) rootView.findViewById(R.id.editData);
        trescEdit = (EditText) rootView.findViewById(R.id.editTresc);
	}

	private void initTextViews() {
		tytul = (TextView) rootView.findViewById(R.id.text_tytul);
        data = (TextView) rootView.findViewById(R.id.text_data);
        tresc = (TextView) rootView.findViewById(R.id.text_tresc);
	}
}