package com.example.listaspraw;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

/**
 * EdycjaActivity - klasa aktywnosc, ktora
 * udostepnia mozliwosc edycji zadan zanjdujacych
 * sie w systemie. Wspolpracuje z layoutem 
 * activity_edycja.
 * @author Kamil
 * @version 201406302226
 */
public class EdycjaActivity extends ActionBarActivity {
	static int numer;
	static Zadanie zadanie;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edycja);
		
		setNumer();
		setZadanie();

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}
	
	private void setNumer() {
		Bundle bundle = getIntent().getExtras();
		numer = Integer.parseInt(bundle.getString("numer"));
	}
	
	private void setZadanie() {
		zadanie = ListaSpraw.getZadanie(numer);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.edycja, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * PlaceholderFragment - klasa dziedziczaca po fragment.
	 * Odpowiedzialna jest za obsluge interfejsu graficznego
	 * w aktywnosci EdycjaActivity. Wspolpracuje z layaoutem
	 * fragment_edycja.
	 * @author Kamil
	 * @version 201406302226
	 */
	public static class PlaceholderFragment extends Fragment {
		View rootView;
		TextView numerTV;
		TextView numerZadaniaTV;
		TextView tytulTV;
		TextView dataTV;
		TextView trescTV;
		EditText tytulEdit;
		EditText dataEdit;
		EditText trescEdit;
		Button zapiszButton;
		CheckBox stanCheckBox;
		
		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			rootView = inflater.inflate(R.layout.fragment_edycja,
					container, false);
			
			initTextViews();
			initEditTextsAndCheckBox();
			initZapiszButton();
			setTextsAndState();
			setTextsAndState();
			
			return rootView;
		}
		
		/**
		 * initZapiszButton - inicjalizacja oblusgi przycisku.
		 */
		private void initZapiszButton() {
			zapiszButton = (Button) rootView.findViewById(R.id.zapiszButton);
	        zapiszButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
					Zadanie nowe = new Zadanie(null, null, null, null);
					Calendar calendar = new GregorianCalendar();
					try {
						calendar.setTime(sdf.parse(dataEdit.getText().toString()));
					} catch (ParseException e) {
						e.printStackTrace();
					}
					nowe.setNazwa(tytulEdit.getText().toString());
					nowe.setTre��(trescEdit.getText().toString());
					nowe.setData(calendar);
					if(stanCheckBox.isChecked()) {
						nowe.setStan(new StanWykonane());						
					} else {
						nowe.setStan(new StanDoWykonania());
					}
					ListaSpraw.zmienZadanie(zadanie, nowe);
					getActivity().finish();
				}
			});
		}
		
		private void setTextsAndState() {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
			Calendar dataZadania = zadanie.getData();
			String textDaty = sdf.format(dataZadania.getTime());
			numerTV.setText("" + numer);
			tytulEdit.setText(zadanie.getNazwa());
			dataEdit.setText(textDaty);
			trescEdit.setText(zadanie.getTre��());
			if(zadanie.getStan().equals((new StanDoWykonania()))){
				stanCheckBox.setChecked(false);
			} else {
				stanCheckBox.setChecked(true);
			}
		}
		
		private void initTextViews() {
			numerTV = (TextView) rootView.findViewById(R.id.numerTextView);
			numerZadaniaTV = (TextView) rootView.findViewById(R.id.numerZadaniaTextView);
			tytulTV = (TextView) rootView.findViewById(R.id.tytulTextView);
			dataTV = (TextView) rootView.findViewById(R.id.dataTextView);
			trescTV = (TextView) rootView.findViewById(R.id.trescTextView);
		}
		
		private void initEditTextsAndCheckBox() {
			tytulEdit = (EditText) rootView.findViewById(R.id.tytulPole);
	        dataEdit = (EditText) rootView.findViewById(R.id.dataPole);
	        trescEdit = (EditText) rootView.findViewById(R.id.trescPole);
	        stanCheckBox = (CheckBox) rootView.findViewById(R.id.stanCheckBox);
		}	
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
	    super.onConfigurationChanged(newConfig);
	    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}
}
