package com.example.listaspraw;

/**
 * Implementacja interfejsu dla stanu
 * zadania jeszcze nie wykonanego.
 * @author Kamil
 * @version 201406302226
 */
class StanDoWykonania implements StanZadania {
	String tekst = "Do Wykonania";
	
	@Override
	public String getNapis() {
		return tekst;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tekst == null) ? 0 : tekst.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof StanDoWykonania)) {
			return false;
		}
		return true;
	}
}