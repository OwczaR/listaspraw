package com.example.listaspraw;


/**
 * Implementacja interfejsu dla stanu
 * zadania juz wykonanego.
 * @author Kamil
 * @version 201406302226
 */
class StanWykonane implements StanZadania {
	String tekst = "Wykonane";
	
	@Override
	public String getNapis() {
		return tekst;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tekst == null) ? 0 : tekst.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof StanWykonane)) {
			return false;
		}
		return true;
	}
}